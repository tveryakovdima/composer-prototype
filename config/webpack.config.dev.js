const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: path.resolve(__dirname, '../src/composer/index.ts'),
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js',
        path: path.resolve(__dirname, '../dist'),
    },
    devtool: 'inline-source-map',
    devServer: {
            contentBase: './dist',
        historyApiFallback: true,
           },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
              },
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ],
        alias: {
            '@App': path.resolve(__dirname, '../src/')
        }
      },
    plugins: [new HtmlWebpackPlugin()]
}
