import { IStreamConfig } from '@App/mocks/streamConfigList';
import { IRootConfig } from '@App/mocks/rootConfigList';


const DEFAULT_ROOT_VERSION = '1'

function loadAppConfig(): Promise<[IRootConfig[], IStreamConfig[]]> {
    const loadRootConfigList = import('@App/mocks/rootConfigList').then(module => module.rootConfigList )
    const loadStreamConfigList = import('@App/mocks/streamConfigList').then(module => module.streamConfigList)

    return Promise.all([loadRootConfigList, loadStreamConfigList])
}

function loadRoot(version: string, rootConfigList: IRootConfig[]) {
    const currentRoot = rootConfigList.find(r => r.version === version);

    return import(`@App/roots/${currentRoot?.url}`)
        .then(module => module.default)
        .catch(error => {console.log('Не удалось загрузить root', error)})
}

function mountRoot(rootVersion: string, rootConfigList: IRootConfig[], streamConfigList: IStreamConfig[]) {
    loadRoot(rootVersion, rootConfigList).then(root => {
        const mismatchCallback = (ver: string) => mountRoot(ver, rootConfigList, streamConfigList) 

        root(streamConfigList, rootVersion, mismatchCallback).mount();
    })
}

function initApp() {
    loadAppConfig().then(values => {
        const [rootConfigList, streamConfigList] = values
        mountRoot(DEFAULT_ROOT_VERSION, rootConfigList, streamConfigList)
    })
}

initApp();
