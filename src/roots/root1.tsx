import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'
import {IStreamConfig} from '@App/mocks/streamConfigList';

interface INavMenuProps {
    routesConfigList: Pick<IStreamConfig, 'name' | 'path'>[]
}

interface ILoadedRoute {
    path: IStreamConfig['path'],
    name: IStreamConfig['name'],
    component: React.ComponentType
}

interface IRootComponentProps {
    routeList: ILoadedRoute[]
}

function NavMenu(props: INavMenuProps) {
    return (
        <>
            {props.routesConfigList.map((l) => <Link style={{marginLeft: '20px'}} key={l.path} to={l.path}>{l.name}</Link>)}
        </>
    )
}

function RootComponent(props:IRootComponentProps) {
    return (
        <BrowserRouter>
            <div>root1</div>
            <NavMenu routesConfigList={props.routeList}/>
            <React.Suspense fallback={<div>loading...</div>}>
                <Switch>
                    {props.routeList.map((r) => <Route key={r?.path} path={r?.path} component={r?.component} exact={true}/>)}
                </Switch>
            </React.Suspense>
    </BrowserRouter>
    )
}

function getMismatchComponent(callback: () => void) {
    return function MismatchedRootVersion() {
        React.useEffect(() => {
            callback()
        }, [])
    
        return <div>mismatched</div>
    }
}

function root1(streamConfigList: IStreamConfig[], version: string, onVersionMismatch: (rootVersion: string) => void, ) {
    const id = `root${version}`

    function handleRootVersionMismatch(version: string) {
        onVersionMismatch(version);
        unMount();
    }

    function mount() {
        const loadedRouteList: ILoadedRoute[] = streamConfigList.map(r => ({
            path: r.path,
            name: r.name,
            component: r.rootVersion === version ? React.lazy(() => import(`@App/streams/${r.url}`)) : getMismatchComponent(() =>handleRootVersionMismatch(r.rootVersion)),
        }))

        let rootDomNode = document.getElementById(id);
        if (!rootDomNode) {
            rootDomNode = document.createElement('div');
            rootDomNode.id = id;
            document.body.appendChild(rootDomNode);
        }
        ReactDOM.render(
            <RootComponent routeList={loadedRouteList}/>,
            rootDomNode
        );
    }

    function unMount() {
        //@ts-ignore
        ReactDOM.unmountComponentAtNode(document.getElementById(id))
    }

    return {
        mount
    }
}

export default root1


