export interface IStreamConfig {
    url: string,
    path: string,
    rootVersion: string 
    name?: string
}

export const streamConfigList: IStreamConfig[] = [
    {url: 'stream1', path: '/stream1', name: 'stream1', rootVersion: '1'},
    {url: 'stream2', path: '/stream2', name: 'stream2', rootVersion: '1'},
    {url: 'stream3', path: '/stream3', name: 'stream3', rootVersion: '2'},
]
