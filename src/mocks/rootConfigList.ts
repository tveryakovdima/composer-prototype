export interface IRootConfig {
    url: string,
    version: string
}

export const rootConfigList: IRootConfig[] = [
    {url: 'root1', version: '1'},
    {url: 'root2', version: '2'},
]